package it.sdep_client.context;

import java.util.LinkedList;
import java.util.List;

public class HubList {
	List<Hub> hubs;

	public HubList(List<Hub> hubs) {
		super();
		this.hubs = hubs;
	}

	public HubList() {
		hubs = new LinkedList<Hub>(); 
	}

	public void add(Hub h)
	{
		hubs.add(h); 
	}
	
	public List<Hub> getHubs()
	{
		return hubs; 
	}

	
	

}
