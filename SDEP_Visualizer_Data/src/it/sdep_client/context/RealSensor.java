package it.sdep_client.context;



public class RealSensor extends Device  {
	public RealSensor(int id) {
		super("No."+id);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Capability of sensor = TYPE of sensor
	 */
	private Capability capability; 
	private int num; 
	private int frequenza=255; 
	private boolean isOn = false;
	
	
	
	public Capability getCapability() {
		return capability;
	}
	public void setCapability(Capability capability) {
		this.capability = capability;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getFrequenza() {
		return frequenza;
	}
	public void setFrequenza(byte frequenza) {
		this.frequenza = frequenza;
	}
	public boolean isOn() {
		return isOn;
	}
	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}
	@Override
	public String getInfos() {
		return "nome:"+this.name+"\n\t\tfreq:"+frequenza+"\n\t\tcapability:"+capability+"\n\t\tisOn:"+isOn+"\n\n";
	}

	

}
