package it.sdep_client.context;

import java.util.LinkedList;
import java.util.List;

public class Hub extends Device	{

	private List<Probe> probes;
	

	public Hub(String id) {
		super(id);
		probes = new LinkedList<Probe>();
	}
	public List<RealSensor> getAllSensors()
	{
		return null; 
	}
	public void addProbe(Probe p) {
		this.probes.add(p); 
	}
	public void removeProbe(int i)
	{
		this.probes.remove(i);
	}
	public List<Probe> getProbes() {
		return probes; 
	}
	@Override
	public String getInfos() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.name);
		for(Probe p : probes)
			builder.append("\n\t"+p.getInfos());
		return builder.toString(); 
		
		
		
		
	}
}
