package it.sdep_client.context;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Probe extends Device {
	
	private List<RealSensor> sensors;
	private int addr;
	
	
	
	 public Probe(int id, int addr) {
		super("STM32_"+id);
		this.addr = addr; 
		sensors = new LinkedList<RealSensor>(); 
	 }

	

	public List<RealSensor> getSensors() {
		return sensors; 
	} 
	/**
	 * 
	 * @author Antonio Guerriero Roberto Maisto Gaetano Perrone  Gaetano Rucco
	 * @return una lista di sensori ordinate per capability
	 */
	public Map<Capability,List<RealSensor>> getSensorsForCapability()
	{
		Map<Capability,List<RealSensor>> capaSensori = new HashMap<Capability,List<RealSensor>>();
		List<List<RealSensor>> listeSensori = new LinkedList<List<RealSensor>>(); 
		List<Capability> capa = new LinkedList<Capability>();
		for(RealSensor s: sensors)
		{
			Capability c = s.getCapability(); 
			if(!capa.contains(c))
				{capa.add(c);
				 listeSensori.add(new LinkedList<RealSensor>());
				}
		}	
		//trova indice della capability
		for(RealSensor s: sensors)
		{
			int index = capa.indexOf(s.getCapability());
			//aggiungi sensore alla lista 
			listeSensori.get(index).add(s); 
		}	
		int i = 0; 
		for(Capability c : capa)
		{
			capaSensori.put(c, listeSensori.get(i++)); 
		}
		
		return capaSensori; 
	}



	public void addSensor(RealSensor s3) {
		sensors.add(s3); 
		
	}



	@Override
	public String getInfos() {
		StringBuilder builder = new StringBuilder(); 
		builder.append(this.name);
		for(RealSensor s: sensors)
		{
			builder.append("\n\t\t");
			builder.append(s.getInfos());
		}
		return builder.toString(); 
		
	}
	
}
