package it.sdep_client.context;

import java.util.List;

public class Capability {
	private String name;
	
	private byte types[] = {0,1,2} ; 
	private int dimensions= 1; 
	
	
	
	public Capability(String name) {
		this.name = name; 
	}

	public Capability(byte type) {
		
		if(type == types[0])
		{
			this.name="ANALOG_IN";
			this.dimensions = 1; 
		}
			
		else if(type == types[1])
		{
			this.name = "DIGITAL_IN"; 
			this.dimensions = 1; 

		}
		else if(type == types[2])
		{
			this.name = "ACCELEROMETER"; 
			this.dimensions = 3; 

		}
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return  name ;
	} 
	
}
