package it.sdep_client.context;

public abstract class Device {
	protected String name;

	public Device(String id) {
		super();
		this.name = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	} 
	
	public abstract String getInfos();

	
}
