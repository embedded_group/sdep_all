package packets;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.*;

public class PacketSender {
	
    private Socket clientSocket;
    

	public PacketSender() {
	}
	
	public String exchangeData(JSONObject o) throws JSONException, UnknownHostException, IOException {
		
		this.clientSocket = new Socket("localhost", Constants.PORT);

		String out = o.toString();
        DataOutputStream outToServer = new DataOutputStream(this.clientSocket.getOutputStream());        
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        
        //NB: la stringa da inviare deve terminare necessariamente con una endline
        outToServer.writeBytes(out+'\n');
        
        String in = inFromServer.readLine();
        
		clientSocket.close(); 
        
        return in;
	
	}
	
	

}
