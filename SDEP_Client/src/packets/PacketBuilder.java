package packets;

import java.io.IOException;
import java.net.UnknownHostException;

import org.json.JSONException;
import org.json.JSONObject;

public class PacketBuilder implements IPacketBuilder {
	
	
	/*************************************************
	 * -------------JSONObject format-----------------
	 *Json_obj {
	 *	"methodName" 	: "nome_metodo",
	 *	"pAddress"	 	: probeAddress,
	 *	"variableField"	: a_parameter,
	 *  "object"		: encapsulated_object
	 * 	}
	 * -----------------------------------------------
	 *************************************************
	 */
	private JSONObject obj;
	
	public PacketBuilder() {
		this.obj = new JSONObject();
	}
	
	/* (non-Javadoc)
	 * @see packets.IPacketBuilder#getProbeCapability(byte)
	 */
	@Override
	public sCapability_tListPacket getProbeCapabilities(byte pAddr) throws UnknownHostException, JSONException, IOException {
		
		this.obj.put(Constants.met, Constants.cap);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
		
		//Server should return only the encapsulated object
		sCapability_tListPacket cap = new sCapability_tListPacket();
		cap.convertJSONToObject(s);
		
		return cap;
	}
	
	
	/* (non-Javadoc)
	 * @see packets.IPacketBuilder#sendProbeConfig(byte, int)
	 */
	@Override
	public byte sendProbeConfig(byte pAddr, sConfig_tListPacket cnList, int scNum)
			throws UnknownHostException, JSONException, IOException {
		
		this.obj.put(Constants.met, Constants.conf);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, scNum);
		this.obj.put(Constants.obj, cnList);
		
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
		
		//Server should return only the encapsulated object
		Byte b = new Byte(s);
		byte res = b.byteValue();
		
		return res;
	}
	
	
	/* (non-Javadoc)
	 * @see packets.IPacketBuilder#getSensorSamples(byte, packets.sId_tPacket)
	 */
	@Override
	public sExchange_tPacket getSensorSamples(byte pAddr, sId_tPacket sensorId) throws Exception {
		
		this.obj.put(Constants.met, Constants.conf);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, sensorId);
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
		
		//Server should return only the encapsulated object
		sExchange_tPacket data = new sExchange_tPacket();
		data.convertJSONToObject(s);
		
		return data;
	}
	
	
	//returns the result of disconnect
	/* (non-Javadoc)
	 * @see packets.IPacketBuilder#sendDisconnect(byte)
	 */
	@Override
	public byte sendDisconnect(byte pAddr) throws Exception {
		
		this.obj.put(Constants.met, Constants.disc);
		this.obj.put(Constants.paddr, pAddr);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
	
		//Server should return only the encapsulated object
		Byte b = new Byte(s);
		byte res = b.byteValue();
		
		return res;
	}
	
	
	/* (non-Javadoc)
	 * @see packets.IPacketBuilder#refreshProbeList()
	 */
	@Override
	public sdepAddressList_fListPacket getProbeList() throws Exception {
	
		this.obj.put(Constants.met, Constants.probes);
		this.obj.put(Constants.paddr, Constants.NULL);
		this.obj.put(Constants.var, Constants.NULL);
		this.obj.put(Constants.obj, Constants.NULL);
		
		//calls getAttachedList()
		PacketSender sender = new PacketSender();
		String s = sender.exchangeData(this.obj);
		
		//Server should return only the encapsulated object
		sdepAddressList_fListPacket res = new sdepAddressList_fListPacket();
		res.convertJSONToObject(s);
		
		return res;
		
	}

}
