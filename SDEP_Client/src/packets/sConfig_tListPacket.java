package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class sConfig_tListPacket implements MyJSON_Interface {
	
	private Vector<sConfig_tPacket> confList;
	
	public sConfig_tListPacket() {
		
	}

	public sConfig_tListPacket(Vector<sConfig_tPacket> confList) {
		super();
		this.confList = confList;
	}

	
	public Vector<sConfig_tPacket> getConfList() {
		return confList;
	}

	public void setConfList(Vector<sConfig_tPacket> confList) {
		this.confList = confList;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {
		
		JSONArray arr = new JSONArray();
		
		for(int i=0; i<confList.size(); i++) {
			JSONObject o = new JSONObject(confList.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException {

		Vector<sConfig_tPacket> v = new Vector<sConfig_tPacket>();
		JSONArray o = new JSONArray(s);
		
		for(int i=0; i<o.length(); i++) {
			JSONObject obj = o.getJSONObject(i);
			sConfig_tPacket c = new sConfig_tPacket();
			c.convertJSONToObject(obj.toString());
			v.add(c);
		}
		
		this.confList = v;		
	}

	@Override
	public String toString() {
		return "sConfig_tListPacket [confList=" + confList + "]";
	}

	
}
