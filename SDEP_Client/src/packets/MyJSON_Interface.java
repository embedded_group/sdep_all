package packets;

import java.util.List;


import org.json.JSONArray;
import org.json.JSONException;

public interface MyJSON_Interface{

	public abstract String convertObjectToJSON() throws JSONException;
	public abstract void convertJSONToObject(String o) throws JSONException; 


	
	
	public static String fillJSON(List<? extends MyJSON_Interface> list) throws JSONException
	{
		
		JSONArray arr = new JSONArray(); 
		for(MyJSON_Interface s : list)
		{
			arr.put(s.convertObjectToJSON()); 
		}
		
		return arr.toString(); 
	}
}
