package packets;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class sCapability_tListPacket implements MyJSON_Interface {
	
	private Vector<sCapability_tPacket> caps;

	public sCapability_tListPacket(Vector<sCapability_tPacket> caps) {
		super();
		this.caps = caps;
	}

	public sCapability_tListPacket() {
		// TODO Auto-generated constructor stub
	}

	public Vector<sCapability_tPacket> getCaps() {
		return caps;
	}

	public void setCaps(Vector<sCapability_tPacket> caps) {
		this.caps = caps;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONArray arr = new JSONArray();
		
		for(int i=0; i<caps.size(); i++) {
			JSONObject o = new JSONObject(caps.get(i).convertObjectToJSON());
			arr.put(o);
		}
		
		return arr.toString();
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException {
		
		Vector<sCapability_tPacket> v = new Vector<sCapability_tPacket>();
		JSONArray o = new JSONArray(s);
		
		for(int i=0; i<o.length(); i++) {
			JSONObject obj = o.getJSONObject(i);
			sCapability_tPacket c = new sCapability_tPacket();
			c.convertJSONToObject(obj.toString());
			v.add(c);
		}
		
		this.caps = v;

	}

	
	//DEBUG
	@Override
	public String toString() {
		return "sCapability_tListPacket [caps=" + caps + "]";
	}
	

}
