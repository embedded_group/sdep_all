package packets;

import org.json.JSONException;
import org.json.JSONObject;

public class sConfig_tPacket implements MyJSON_Interface {
	
	private sId_tPacket sensorId;
	private byte sensorMode;
	private byte sensorPeriod;
	
	
	public sConfig_tPacket() {
		super();
	}


	public sConfig_tPacket(sId_tPacket sensorId, byte sensorMode, byte sensorPeriod) {
		super();
		this.sensorId = sensorId;
		this.sensorMode = sensorMode;
		this.sensorPeriod = sensorPeriod;
	}


	public sId_tPacket getSensorId() {
		return sensorId;
	}


	public void setSensorId(sId_tPacket sensorId) {
		this.sensorId = sensorId;
	}


	public byte getSensorMode() {
		return sensorMode;
	}


	public void setSensorMode(byte sensorMode) {
		this.sensorMode = sensorMode;
	}


	public byte getSensorPeriod() {
		return sensorPeriod;
	}


	public void setSensorPeriod(byte sensorPeriod) {
		this.sensorPeriod = sensorPeriod;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject obj = new JSONObject(); 
		
		sId_tPacket id = sensorId;
		JSONObject idJson = new JSONObject(id.convertObjectToJSON());
	
		obj.put(Constants.SENS_ID, idJson); 
		obj.put(Constants.CON_MODE, this.sensorMode); 
		obj.put(Constants.CON_PER, this.sensorPeriod);

		return obj.toString(); 
		
	}


	@Override
	public void convertJSONToObject(String o) throws JSONException {
		
		sId_tPacket id = new sId_tPacket();		
		JSONObject obj = new JSONObject(o);
		
		JSONObject json_id = obj.getJSONObject(Constants.SENS_ID);
		
		id.convertJSONToObject(json_id.toString());
		
		this.sensorId = id; 
		
		this.sensorMode = (byte)obj.getInt(Constants.CON_MODE);
		this.sensorPeriod = (byte)obj.getInt(Constants.CON_PER);

	}


	@Override
	public String toString() {
		return "sConfig_tPacket [sensorId=" + sensorId + ", sensorMode="
				+ sensorMode + ", sensorPeriod=" + sensorPeriod + "]";
	}
	
	
	
	
	

}
