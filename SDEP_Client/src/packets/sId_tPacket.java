package packets;

import org.json.JSONException;
import org.json.JSONObject;

public class sId_tPacket implements MyJSON_Interface {
	
	private byte sensorType;
	private byte sensorNum;
	
	
	public sId_tPacket(byte sensorType, byte sensorNum) {
		super();
		this.sensorType = sensorType;
		this.sensorNum = sensorNum;
	}


	public sId_tPacket() {
		// TODO Auto-generated constructor stub
	}


	public byte getSensorType() {
		return sensorType;
	}


	public void setSensorType(byte sensorType) {
		this.sensorType = sensorType;
	}


	public byte getSensorNum() {
		return sensorNum;
	}


	public void setSensorNum(byte sensorNum) {
		this.sensorNum = sensorNum;
	}


	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject o = new JSONObject(); 
		
		o.put(Constants.SENS_TYPE, this.sensorType); 
		o.put(Constants.SENS_NUM, this.sensorNum);  		
		
		return o.toString(); 
		
	}


	@Override
	public void convertJSONToObject(String s) throws JSONException {

		JSONObject o = new JSONObject(s);
		this.sensorType = (byte)o.getInt(Constants.SENS_TYPE);
		this.sensorNum = (byte)o.getInt(Constants.SENS_NUM); 
		
	}


	@Override
	public String toString() {
		return "sId_tPacket [sensorType=" + sensorType + ", sensorNum="
				+ sensorNum + "]";
	};
	
	
	
	
	
	
	

}
