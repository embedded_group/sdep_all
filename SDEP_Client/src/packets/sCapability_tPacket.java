package packets;

import org.json.JSONException;
import org.json.JSONObject;

import packets.Constants;

public class sCapability_tPacket implements MyJSON_Interface {
	
	
	private byte sensorType;
	private byte sensorQuantity;
	private byte sampleLen;

	
	public sCapability_tPacket() {
	}


	public sCapability_tPacket(byte st, byte sq, byte sl) {
		super();
		
		sensorType = st;
		sensorQuantity = sq;
		sampleLen = sl;
		
	}

	public byte getSensorType() {
		return sensorType;
	}

	public void setSensorType(byte sensorType) {
		this.sensorType = sensorType;
	}

	public byte getSensorQuantity() {
		return sensorQuantity;
	}

	public void setSensorQuantity(byte sensorQuantity) {
		this.sensorQuantity = sensorQuantity;
	}

	public byte getSampleLen() {
		return sampleLen;
	}

	public void setSampleLen(byte sampleLen) {
		this.sampleLen = sampleLen;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {
		
		JSONObject o = new JSONObject(); 
	
		o.put(Constants.SENS_TYPE, this.sensorType); 
		o.put(Constants.CAP_QUAN, this.sensorQuantity); 
		o.put(Constants.CAP_LEN,this.sampleLen); 		
		
		return o.toString(); 
		
	}

	@Override
	public void convertJSONToObject(String s) throws JSONException {
		
		JSONObject o = new JSONObject(s);
		this.sensorType = (byte)o.getInt(Constants.SENS_TYPE);
		this.sensorQuantity = (byte)o.getInt(Constants.CAP_QUAN); 
		this.sampleLen = (byte)o.getInt(Constants.CAP_LEN); 
	
	}

	
}
