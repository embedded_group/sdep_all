package packets;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

public class Test {

	public static void main(String[] args) throws Exception {

		PacketBuilder pktBdr = new PacketBuilder();
		
		/*sCapability_tListPacket capList = new sCapability_tListPacket();
		capList = pktBdr.getProbeCapabilities((byte)23);
		System.out.println("Res from "+Constants.cap+": "+capList.toString());*/
		
		
		sId_tPacket id1 = new sId_tPacket((byte)11,(byte)43);
		sId_tPacket id2 = new sId_tPacket((byte)2,(byte)24);
		sConfig_tPacket c1 = new sConfig_tPacket(id1,(byte)32,(byte)44);
		sConfig_tPacket c2 = new sConfig_tPacket(id2,(byte)7,(byte)55);
		Vector<sConfig_tPacket> v = new Vector<sConfig_tPacket>();
		v.add(c1);
		v.add(c2);
		
		sConfig_tListPacket confList = new sConfig_tListPacket(v);
		byte res = pktBdr.sendProbeConfig((byte)3, confList, 2);
		System.out.println("Res from "+Constants.conf+": "+res);
		
		
		/*sExchange_tPacket data = new sExchange_tPacket();
		data = pktBdr.getSensorSamples((byte)32, id1);
		System.out.println("Res from "+Constants.sampl+": "+data.toString());
		
		
		byte res2 = pktBdr.sendDisconnect((byte)32);
		System.out.println("Res from "+Constants.disc+": "+res2);
		
		sdepAddressList_fListPacket probes = new sdepAddressList_fListPacket();
		probes = pktBdr.getProbeList();
		System.out.println("Res from "+Constants.disc+": "+probes);
	*/
		
		
		
		
		/*
		byte x1=2, x2=3, x3=4;
		sCapability_tPacket c1 = new sCapability_tPacket(x1,x2,x3);
		
		byte x11=21, x21=31, x31=41;
		sCapability_tPacket c2 = new sCapability_tPacket(x11,x21,x31);
		
		byte x12=22, x22=32, x32=42;
		sCapability_tPacket c3 = new sCapability_tPacket(x12,x22,x32);
		
		Vector<sCapability_tPacket> v = new Vector<sCapability_tPacket>();
		v.add(c1);
		v.add(c2);
		v.add(c3);
		
		sCapability_tListPacket c = new sCapability_tListPacket(v);
		
		String s = new String(c.convertObjectToJSON());
		
		System.out.println(s);		
		
		sCapability_tListPacket cc = new sCapability_tListPacket();
		cc.convertJSONToObject(s);
		
		System.out.println("\n");
		System.out.println(cc);		
		
		JSONObject o = new JSONObject();
		o.put("sesso", 23);
		o.put("cane", 25);
		System.out.println(o);
		
		o.put("sesso", 5);
		System.out.println(o);
		*/
		
		
	}

}
