package packets;

public class Constants {
	
	/*JSON OBJECTS KEYS*/
	public static final String met = "methodName";
	public static final String paddr = "pAddress";
	public static final String obj = "object";
	public static final String cap = "sendCapabilityRequest";
	public static final String conf = "sendConfigTo";
	public static final String probes = "getAttachedList";
	public static final String disc = "sendDisconnect";
	public static final String sampl = "sendSamplesRequest";
	
	//variable field used to send parameters
	public static final String var = "variableField";
	public static final String NULL = "null";
	
	/*sCapability_t KEYS*/
	public static final String CAP_QUAN = "sensorQuantity";
	public static final String CAP_LEN = "sampleLen";
	public static final String CAP_LIST = "sCapability_tList";
	
	/*sConfig_t KEYS*/
	public static final String SENS_ID = "sensorID";
	public static final String CON_MODE = "sensorMode";
	public static final String CON_PER = "sensorPeriod";
	
	/*sId_t KEYS*/
	public static final String SENS_NUM = "sensorNum";
	public static final String SENS_TYPE = "sensorType";
	
	/*sError_t KEYS*/
	public static final String ERR_CODE = "errorCode";
	
	/*sExchange_t KEYS*/
	public static final String SAM_PKT = "samplesPacket";
	
	/*Socket PORT*/
	public static final int PORT = 7098;
	
	/*sdepAddressList_f KEYS*/
	public static final String PSER = "pSerial";

}
