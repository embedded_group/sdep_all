package packets;

import org.json.JSONException;
import org.json.JSONObject;

public class sError_tPacket implements MyJSON_Interface{
	
	private sId_tPacket sensorId;
	private byte errorCode;
	
	public sError_tPacket() {

	}

	public sError_tPacket(sId_tPacket sensorId, byte errorCode) {
		super();
		this.sensorId = sensorId;
		this.errorCode = errorCode;
	}

	public sId_tPacket getSensorId() {
		return sensorId;
	}

	public void setSensorId(sId_tPacket sensorId) {
		this.sensorId = sensorId;
	}

	public byte getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(byte errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String convertObjectToJSON() throws JSONException {

		JSONObject obj = new JSONObject(); 
		
		sId_tPacket id = sensorId;
		JSONObject idJson = new JSONObject(id.convertObjectToJSON());
	
		obj.put(Constants.SENS_ID, idJson); 
		obj.put(Constants.ERR_CODE, this.errorCode); 

		return obj.toString(); 
	}

	@Override
	public void convertJSONToObject(String o) throws JSONException {

		sId_tPacket id = new sId_tPacket();		
		JSONObject obj = new JSONObject(o);
		
		JSONObject json_id = obj.getJSONObject(Constants.SENS_ID);
		
		id.convertJSONToObject(json_id.toString());
		
		this.sensorId = id; 
		
		this.errorCode = (byte)obj.getInt(Constants.ERR_CODE);

	}
	
	
	
	

}
