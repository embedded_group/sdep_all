package packets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTest {

	public static void main(String[] args) throws IOException {
		
		String clientIn; 
		String capitalizedSentence;
		ServerSocket welcomeSocket = new ServerSocket(Constants.PORT); 
		
		while(true) {
			System.out.println("[SERVER] Waiting...\n");
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(
								connectionSocket.getInputStream())); 
			DataOutputStream outToClient = new DataOutputStream(
								connectionSocket.getOutputStream()); 
			
			clientIn = inFromClient.readLine();
			
			System.out.println("[SERVER] Received: "+clientIn+"\n");
			
			/*capitalizedSentence = clientIn.toUpperCase()+ "\n"; 
			outToClient.writeBytes(capitalizedSentence);*/
		
		}
	}

}
