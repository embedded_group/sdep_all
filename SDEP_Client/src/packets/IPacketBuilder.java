package packets;

import org.json.JSONException;

public interface IPacketBuilder {

	public abstract sCapability_tListPacket getProbeCapabilities(byte pAddr)
			throws JSONException, Exception;

	public abstract byte sendProbeConfig(byte pAddr, sConfig_tListPacket cnList, int scNum)
			throws JSONException, Exception;

	public abstract sExchange_tPacket getSensorSamples(byte pAddr,
			sId_tPacket sensorId) throws JSONException, Exception;

	//returns the result of disconnect
	public abstract byte sendDisconnect(byte pAddr) throws JSONException, Exception;

	public abstract sdepAddressList_fListPacket getProbeList() throws JSONException, Exception;

}