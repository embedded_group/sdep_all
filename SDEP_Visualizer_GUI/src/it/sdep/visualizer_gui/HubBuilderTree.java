package it.sdep.visualizer_gui;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import it.sdep_client.context.Capability;
import it.sdep_client.context.Hub;
import it.sdep_client.context.HubList;
import it.sdep_client.context.Probe;
import it.sdep_client.context.RealSensor;

public class HubBuilderTree {

		private List<Hub> hubs;

		public HubBuilderTree(HubList hubs)
		{
			this.hubs = hubs.getHubs(); 
		}
		/**
		 * Genera l'albero dei nodi grafico a partire da un hub list
		 * @author Antonio Guerriero Roberto Maisto Gaetano Perrone  Gaetano Rucco
		 * @return
		 */
		public DefaultMutableTreeNode create()
		{
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("SDEP System");
			
		
			for(Hub h : hubs)
			{
				DefaultMutableTreeNode hubNode = new DefaultMutableTreeNode(h); 
				List<Probe> sonde = h.getProbes(); 
				for(Probe s : sonde)
				{
					DefaultMutableTreeNode probeNode = new DefaultMutableTreeNode(s); 
					Map<Capability,List<RealSensor>> capRS =s.getSensorsForCapability(); 
					Set<Capability> capas = capRS.keySet(); 
					for(Capability c: capas)
					{
						DefaultMutableTreeNode capaNode = new DefaultMutableTreeNode(c); 
						List<RealSensor> sensoriDiCapa = capRS.get(c); 
						for(RealSensor singoloS: sensoriDiCapa)
						{
							DefaultMutableTreeNode sensorNode = new DefaultMutableTreeNode(singoloS);
							capaNode.add(sensorNode);
						}
						probeNode.add(capaNode);
					}
					
					
					hubNode.add(probeNode);
					
				}
				root.add(hubNode);
			}
			return root;
		}
	
		
		
		
		
	
}
