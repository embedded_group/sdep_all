package it.sdep.visualizer_gui;

import it.sdep_client.context.RealSensor;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.SpinnerNumberModel;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.border.TitledBorder;
import javax.swing.JSpinner;

public class PanelConfigSensor extends JPanel{
	private RealSensor sensore;
	private JSpinner spinner;
	public PanelConfigSensor(RealSensor s) {
		
		
		setLayout(new GridLayout(2, 1, 0, 0));
		this.sensore = s; 
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Period", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_2);
		
		JPanel panel = new JPanel();
		panel_2.add(panel);
		
		
		SpinnerNumberModel model1 = new SpinnerNumberModel(250, 1, 250, 1);
		 spinner = new JSpinner(model1);
		//spinner.setEditor(new JSpinner.DefaultEditor(spinner));
		panel.add(spinner);
		
		JLabel lblNewLabel = new JLabel("Ms(1-255)");
		panel.add(lblNewLabel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Other", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		add(panel_3);
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);
	}
	
	public JSpinner getSpinner()
	{
		return spinner; 
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2430304955118037795L;

}
