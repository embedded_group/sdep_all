package it.sdep.visualizer_gui;

import javax.swing.tree.DefaultMutableTreeNode;


public interface ITreeListModel<T>{
	public T getModel();

}
