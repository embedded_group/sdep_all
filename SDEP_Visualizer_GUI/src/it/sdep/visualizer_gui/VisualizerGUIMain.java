package it.sdep.visualizer_gui;
import it.giper.guiutils.GUI_Utils;
import it.sdep.visualizer_bl.IBusinessVisualizerGUI;
import it.sdep.visualizer_bl.StubBusinessVisualizer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTree;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.Box;
import javax.swing.JButton;

import java.awt.Component;

import javax.swing.JMenuBar;
import javax.swing.JMenu;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class VisualizerGUIMain {

	private JFrame frame;
	private PanelTree panelTree;
	static IBusinessVisualizerGUI bl = new StubBusinessVisualizer(); 
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisualizerGUIMain window = new VisualizerGUIMain();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VisualizerGUIMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("SDEP Visualizer");
		GUI_Utils.initLookAndFeel("Metal", "");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		panelTree = new PanelTree((String) "panelTree");
		scrollPane.setViewportView(panelTree);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox = Box.createHorizontalBox();
		horizontalBox.add(Box.createHorizontalStrut(50));
		panel.add(horizontalBox, BorderLayout.WEST);
		
		JButton btnNewButton = new JButton("Active");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				activeButtonPressed();
			}

			private void activeButtonPressed() {
					VisualizerGUIMain.bl.active();
			}
		});
		panel.add(btnNewButton);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		panel.add(horizontalBox_1, BorderLayout.EAST);
		
		Component horizontalStrut = Box.createHorizontalStrut(50);
		horizontalBox_1.add(horizontalStrut);
		
		JMenuBar menuBar = new JMenuBar();
		frame.getContentPane().add(menuBar, BorderLayout.NORTH);
		
		JMenu mnInfo = new JMenu("Info");
		menuBar.add(mnInfo);
		
		
		initTreeList(); 
	}

	private void initTreeList() {
	}

}
