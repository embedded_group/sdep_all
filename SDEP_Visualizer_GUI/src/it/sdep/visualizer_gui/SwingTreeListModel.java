package it.sdep.visualizer_gui;

import javax.swing.tree.DefaultMutableTreeNode;

public class SwingTreeListModel implements ITreeListModel<DefaultMutableTreeNode> {

	private DefaultMutableTreeNode tn; 
	
	public SwingTreeListModel(String name) {
		tn = new DefaultMutableTreeNode(name) ; 
	}

	public SwingTreeListModel() {
		tn = new DefaultMutableTreeNode() ; 
	}

	public void set(DefaultMutableTreeNode defaultMutableTreeNode) {
		this.tn = defaultMutableTreeNode; 
	}

	@Override
	public DefaultMutableTreeNode getModel() {
		return tn; 
	}

}
