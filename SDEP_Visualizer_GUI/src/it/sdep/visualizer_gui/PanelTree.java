package it.sdep.visualizer_gui;
import it.giper.guiutils.MyJPanel;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

public class PanelTree extends MyJPanel{

	private JTree tree;

	public PanelTree(String name) {
		super(name);
		setLayout(new BorderLayout(0, 0));
		
		tree = new JTree(VisualizerGUIMain.bl.generateTreeListModel().getModel());
		tree.addMouseListener(new TreeListener(tree));
		tree.setCellRenderer(new DefaultTreeCellRenderer()
        {
             /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Component getTreeCellRendererComponent(JTree pTree,
                 Object pValue, boolean pIsSelected, boolean pIsExpanded,
                 boolean pIsLeaf, int pRow, boolean pHasFocus)
             {
	    DefaultMutableTreeNode nodeBase = (DefaultMutableTreeNode)pValue;
	    super.getTreeCellRendererComponent(pTree, pValue, pIsSelected,
                     pIsExpanded, pIsLeaf, pRow, pHasFocus);
                 if (nodeBase.isLeaf())
                	 setIcon(new ImageIcon("images/off.png"));
//	    else if (nodeBase.getChildCount() > 0)
//	       setBackgroundSelectionColor(Color.yellow);
//	    else if (pIsLeaf)
//	       setBackgroundSelectionColor(Color.green);
	    return (this);
	}
        });
		add(tree);
		
	}

	public void updateTree(DefaultMutableTreeNode mtn)
	{
		tree = new JTree(mtn); 
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * 
	 * 
	 */
	private static final long serialVersionUID = -9092717967869275299L;

}
