package it.sdep.visualizer_gui;

import it.giper.guiutils.GUI_Utils;
import it.sdep_client.context.RealSensor;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

public class TreeListener extends MouseAdapter{
    private JTree _Tree;
    private boolean singleClick  = true;
    private int doubleClickDelay = 300;
    private Timer timer;
	private Object objUnderCursor;    

	
		/**
		 * Gestore di eventi col mouse sull'albero delle stm 
		 * @param tree
		 *
		 */
    public TreeListener(JTree tree)
    {
        this._Tree = tree;
        ActionListener actionListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {                
                timer.stop();
                if (singleClick) {
                    singleClickHandler(e);
                } else {
                    try {
                        doubleClickHandler(e);
                    } catch (ParseException ex) {
                        Logger.getLogger(ex.getMessage());
                    }
                }
            }
        }; 
        timer = new javax.swing.Timer(doubleClickDelay, actionListener);
        timer.setRepeats(false);
    }

    public void mouseClicked(MouseEvent e) { 
    	TreePath path = _Tree.getPathForLocation(e.getX(), e.getY());
    	if(path != null)
    	{
    	int cnt= path.getPathCount();
    	objUnderCursor = path.getPath()[cnt-1];

    	if(SwingUtilities.isRightMouseButton(e))
    		manageRightClick(); 
    	
    	
    	
    	
        if (e.getClickCount() == 1) {
            singleClick = true;
            timer.start();
        } else {
            singleClick = false;
        }
    	}
    }

    //Se premi tasto destro apri pannello configurazione sensore 
    private void manageRightClick() {
    	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) objUnderCursor;
    	 if(!node.isLeaf()) return; 
    	 
    	 RealSensor s = (RealSensor) node.getUserObject();
    	 if(!s.isOn()) GUI_Utils.errorGUI((JPanel)_Tree.getParent(), "Pls accendi prima il sensore");
    	
    	 else{
    	 //GUI_Utils.notifyGUI((JPanel) _Tree.getParent(), "Hello "+s);
    		 PanelConfigSensor pcs = new PanelConfigSensor(s); 
    		 JOptionPane.showMessageDialog(null, pcs);
    		 s.setFrequenza(((Integer)pcs.getSpinner().getValue()).byteValue());
    	 }
     }

	private void singleClickHandler(ActionEvent e) {
        System.out.println("-- single click --");
    }

     private void doubleClickHandler(ActionEvent e) throws ParseException {         
    	 DefaultMutableTreeNode node = (DefaultMutableTreeNode) objUnderCursor;
    	 if(node.isLeaf())
    		 changeState(node);

//    	 System.out.println(node.getUserObject().getClass());
    }

	private void changeState(DefaultMutableTreeNode node) {
		
		
		RealSensor s = (RealSensor) node.getUserObject();
		ImageIcon toShow ; 
		if(s.isOn())
		{
			//Bisogna spegnerlo 
			s.setOn(false);
			toShow = new ImageIcon("images/off.png");
		}
		else 
		{
			s.setOn(true);
			toShow = new ImageIcon("images/on.png"); 
		}
		
		
   	 _Tree.setCellRenderer(new DefaultTreeCellRenderer()
        {
   		 
   		 
             /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Component getTreeCellRendererComponent(JTree pTree,
                 Object pValue, boolean pIsSelected, boolean pIsExpanded,
                 boolean pIsLeaf, int pRow, boolean pHasFocus)
             {
	    DefaultMutableTreeNode nodeBase = (DefaultMutableTreeNode)pValue;
	    super.getTreeCellRendererComponent(pTree, pValue, pIsSelected,
                     pIsExpanded, pIsLeaf, pRow, pHasFocus);
	    	if(nodeBase.isLeaf())
                 if (nodeBase.equals(node))
                	 setIcon(toShow); 
                 else
                 {
                	 RealSensor sensor = (RealSensor)nodeBase.getUserObject(); 
                	 setIcon(sensor.isOn()?new ImageIcon("images/on.png") : new ImageIcon("images/off.png")); 
                 }
	    return (this);
	}
        });
	}     
}