package it.sdep.visualizer_bl;

import java.util.Vector;

import org.json.JSONException;

import packets.IPacketBuilder;
import packets.sCapability_tListPacket;
import packets.sCapability_tPacket;
import packets.sdepAddressList_fListPacket;
import packets.sdepAddressList_fPacket;
import it.sdep_client.context.*;
public class DomainPacketConverter {

	private IPacketBuilder iPacketBuilder;
	
	
	
	
	public DomainPacketConverter() throws Exception
	{
		
		sdepAddressList_fListPacket listPackets=   iPacketBuilder.getProbeList();
		 Vector<sdepAddressList_fPacket> pacchettiProbe = listPackets.getAddrs(); 
		for(int i = 0 ;  i < pacchettiProbe.size() ; i++)
		{
			 sdepAddressList_fPacket pacchetto = pacchettiProbe.get(i); 
			int seriale = pacchetto.getpSerial();
			byte id = pacchetto.getpAddr(); 
			Probe p = new Probe(seriale,id); 
			
			
			
			sCapability_tListPacket capaPackets = iPacketBuilder.getProbeCapabilities(id);
			Vector<sCapability_tPacket> capabilitiesPack = capaPackets.getCaps();
			for(sCapability_tPacket c: capabilitiesPack)
			{
				byte type = c.getSensorType();
				Capability capa = new Capability(type);
				byte num = c.getSensorQuantity();
			
				for(byte j = 0; j < num; j++)
				{
					RealSensor s = new RealSensor(j); 
					s.setCapability(capa);
					s.setNum(j);
				}
			}
		
		
		}
		
	}
	
	
	
	

	
}
	
	
	
	

