package it.sdep.visualizer_bl;

import it.sdep.visualizer_gui.ITreeListModel;
import it.sdep_client.context.Hub;
import it.sdep_client.context.HubList;
import it.sdep_client.context.Probe;

import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

public interface IBusinessVisualizerGUI {
	
	/**
	 * Get list of hubs by server
	 * @author Antonio Guerriero
	 *		   Roberto Maisto
	 *		   Gaetano Perrone
	 *		   Gaetano Rucco
	 *
	 * @return
	 */
	public HubList getListHubs();


	
	/**
	 * Initialize a probe with the setted params 
	 * @author Antonio Guerriero Roberto Maisto Gaetano Perrone  Gaetano Rucco
	 * @param p
	 */
	public void configProbe(Probe probe);

	public ITreeListModel<DefaultMutableTreeNode> generateTreeListModel();
	
	
	/**
	 * Send to server a request to active some sensors 
	 * @author Antonio Guerriero Roberto Maisto Gaetano Perrone  Gaetano Rucco
	 */
	void active(); 
	
	
}
