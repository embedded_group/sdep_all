package it.sdep.visualizer_bl;

import it.sdep.visualizer_gui.HubBuilderTree;
import it.sdep.visualizer_gui.ITreeListModel;
import it.sdep.visualizer_gui.SwingTreeListModel;
import it.sdep_client.context.Capability;
import it.sdep_client.context.Hub;
import it.sdep_client.context.HubList;
import it.sdep_client.context.Probe;
import it.sdep_client.context.RealSensor;

import java.util.LinkedList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

public class StubBusinessVisualizer implements IBusinessVisualizerGUI {

	HubList hubs; 
	
	public StubBusinessVisualizer()
	{
		hubs= new HubList();
		Probe p1 = new Probe(1,2);
		Probe p2 = new Probe(2,3);
		Capability c = new Capability("Accelerometro"); 
		RealSensor s1 = new RealSensor(1); 
		RealSensor s2 = new RealSensor(2); 
		s1.setCapability(c);
		s2.setCapability(c);
		
		Capability c2 = new Capability("Magnetometro"); 
		RealSensor s3 = new RealSensor(1);
		RealSensor s4 = new RealSensor(2);
		s3.setCapability(c2);
		s4.setCapability(c2);
		Hub h = new Hub("Hub_No1");
		p1.addSensor(s3);
		p1.addSensor(s1);
		p2.addSensor(s4);
		p2.addSensor(s2);
		
		
		h.addProbe(p1); 
		h.addProbe(p2);
		hubs.add(h);
	
	}
	
	@Override
	public ITreeListModel<DefaultMutableTreeNode> generateTreeListModel()
	{
		SwingTreeListModel m = new SwingTreeListModel(); 
		HubBuilderTree builderTree = new HubBuilderTree(hubs); 
		DefaultMutableTreeNode tree =builderTree.create(); 
		m.set(tree); 
		return m; 
	}
	
	
	@Override
	public HubList getListHubs() {
		return hubs; 
	}

	@Override
	public void active() {
		for(Hub h:hubs.getHubs())
		{
			System.out.println(h.getInfos());
		}
	}

	@Override
	public void configProbe(Probe probe) {
		System.out.println("Probe to configure "+probe); 
		
	}

}

 