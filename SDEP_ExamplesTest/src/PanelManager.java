

import java.awt.Container;
import java.util.LinkedList;

import javax.swing.JPanel;

public class PanelManager {

	
	private static PanelManager istance;
	private LinkedList<MyJPanel> listPanels; 
	private PanelManager()
	{
		listPanels = new LinkedList<MyJPanel>(); 
		
	}
	
	public static PanelManager getIstance()
	{
		if(istance == null)
			istance = new PanelManager();
		 return istance;
	}
	
	public void addPanel(MyJPanel myJPanel)
	{
		listPanels.add(myJPanel); 
	}
	
	public void removePanel(JPanel panel)
	{
		listPanels.remove(panel); 
	}
	
	
	public void switchPanel(String classToVisible)
	{
		for(MyJPanel c: listPanels)
		{
			if(c.getClass().getName() == classToVisible)
			{
				c.inizialize();
				c.setVisible(true);
			
			}	
			else c.setVisible(false);
		}
	}
	
}
