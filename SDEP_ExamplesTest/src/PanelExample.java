import javax.swing.JTree;
import java.awt.BorderLayout;

public class PanelExample extends MyJPanel{

	public PanelExample(String name) {
		super(name);
		setLayout(new BorderLayout(0, 0));
		
		JTree tree = new JTree();
		add(tree);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -9092717967869275299L;

}
