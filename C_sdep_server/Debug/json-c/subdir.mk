################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../json-c/arraylist.o \
../json-c/debug.o \
../json-c/json_c_version.o \
../json-c/json_object.o \
../json-c/json_object_iterator.o \
../json-c/json_tokener.o \
../json-c/json_util.o \
../json-c/linkhash.o \
../json-c/printbuf.o \
../json-c/random_seed.o 

C_SRCS += \
../json-c/arraylist.c \
../json-c/debug.c \
../json-c/json_c_version.c \
../json-c/json_object.c \
../json-c/json_object_iterator.c \
../json-c/json_tokener.c \
../json-c/json_util.c \
../json-c/libjson.c \
../json-c/linkhash.c \
../json-c/printbuf.c \
../json-c/random_seed.c 

OBJS += \
./json-c/arraylist.o \
./json-c/debug.o \
./json-c/json_c_version.o \
./json-c/json_object.o \
./json-c/json_object_iterator.o \
./json-c/json_tokener.o \
./json-c/json_util.o \
./json-c/libjson.o \
./json-c/linkhash.o \
./json-c/printbuf.o \
./json-c/random_seed.o 

C_DEPS += \
./json-c/arraylist.d \
./json-c/debug.d \
./json-c/json_c_version.d \
./json-c/json_object.d \
./json-c/json_object_iterator.d \
./json-c/json_tokener.d \
./json-c/json_util.d \
./json-c/libjson.d \
./json-c/linkhash.d \
./json-c/printbuf.d \
./json-c/random_seed.d 


# Each subdirectory must supply rules for building sources it contributes
json-c/%.o: ../json-c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


