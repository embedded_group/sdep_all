#include "server_f.h"


void initServer(int* listenfd, int *confd)
{
	struct sockaddr_in serv_addr;

		uint8_t buf[158];

		memset(&buf, '0', sizeof(buf));
		*listenfd = socket(AF_INET, SOCK_STREAM, 0);

		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		serv_addr.sin_port = htons(7098);

		bind(*listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
		printf("binding\n");
}

int main()
{

	int listenfd = 0, connfd = 0;   //related with the server

	initServer(&listenfd,&connfd);

    ssize_t r;

    char buff[1000];
    int expr=CLOSE;
    uint8_t pAddress;
    json_object *jTemppA;

    while(1){
    	listen(listenfd, 1);
    	printf("listening\n");
    	connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);

        r = recv(connfd, buff, 1000,0);

        if (r == -1){
            perror("read");
            return EXIT_FAILURE;
        }

    	json_object * jobj =json_tokener_parse(buff);


    	json_object *jTempM = json_object_new_object();
    	jTempM = json_object_object_get(jobj,"Method");
        if (jTempM == NULL)
        		printf("ERROR: field Method not found!\n");
        else{
    		expr=json_object_get_int(jTempM);
        	json_object_object_del(jobj, "Method");
        }


        jTemppA = json_object_object_get(jobj,"pAddress");
        if (jTemppA == NULL)
               	printf("ERROR: field pAddress not found!\n");
        else{
        	pAddress=json_object_get_int(jTemppA);
        	json_object_object_del(jobj, "pAddress");
        }


        switch (expr) {
           	    case C_ALIVE:
           	    	printf("checkAlive\n");
           	    	//checkAlive(pAddress);
           	    		break;
           	    case S_DISCONNECT:
           	    	printf("sendDisconnect\n");
           	    	//sendDisconnect(pAddress);
           	            break;
           	    case S_CAPABILITY:
           	    	printf("sendCapabilityRequest\n");

           	    	sCapability_t a[2];

           	    	a[0].sampleLen = 1;
           	    	a[0].sensorQuantity = 2;
           	    	a[0].sensorType = 3;

           	    	a[1].sampleLen = 4;
           	    	a[1].sensorQuantity = 5;
           	    	a[1].sensorType = 6;

           	    	capabilityResponse (pAddress, a, 2);
           	    	//sendCapabilityRequest(pAddress);
           	    	char temp_buff[1000];

           	    	    if (strcpy(temp_buff, json_object_to_json_string(jresp)) == NULL)
           	    	    {
           	    	        perror("strcpy");
           	    	        return EXIT_FAILURE;
           	    	    }

           	    	    printf(" stringa da mandare %s\n",temp_buff);

           	    	//    if (write(connfd, temp_buff, strlen(temp_buff)) == -1)

               	    if (send(connfd, temp_buff, strlen(temp_buff),0) == -1)

           	    	    {
           	    	        perror("write");
           	    	        return EXIT_FAILURE;
           	    	    }
               	    	printf("Stringa mandata\n");

           	            break;
           	    case S_CONFIG:
           	    	printf("sendConfigTo\n");
//           	    	jTemppA = json_object_object_get(jobj,"scNum");
//
//           	    	if (jTemppA == NULL)
//           	    	       		printf("ERROR: field scNum not found!");
//
//           	    	json_object_object_del(jobj, "scNum");
//
//           	    	uint16_t scNum =json_object_get_int(jTemppA);
//
//           	    	sConfig_t *configList;
//           	    	jSon2sConfig_tList(jobj, &configList);
//
//           	    	sendConfigTo(pAddress, configList, scNum);
      	            break;
           	    case S_SAMPLE_REQ:
           	    	printf("sendSamplesRequest\n");
//           	    	sId_t *sensorId;
//           	    	jSon2sId_t(jobj, &sensorId);
//           	    	sendSamplesRequest(pAddress, sensorId);
           	    	break;
           	    case CLOSE:
           	    	printf("CHIUSURA DEL SERVER\n");
           	    	return 0;
           	    	break;
        	    }

//        printf("Size of JSON object- %lu\n", sizeof(jobj));
//        printf("Size of JSON_TO_STRING- %lu,\n %s\n", sizeof(json_object_to_json_string(jobj)), json_object_to_json_string(jobj));

    }

    printf("FINE: %s\n", buff);

    return EXIT_SUCCESS;
}
