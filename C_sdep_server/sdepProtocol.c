/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "sdepProtocol.h"
#include "sdepMessage.h"
#include "sdepCore.h"

extern sdepCore_t sdepCore;

/**
 * @brief	Send a AREYOUALIVE message to the Slave
 * @param	pAddress - address of selected probe where to send and receive
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t checkAlive(mAddress_t pAddress){
	sdepMessage_t *message;
	uint16_t pLength;
	uint32_t txByte;
	
	int8_t retVal = -sdepERROR;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	pLength = 0;
		
	message = newMessage(AREYOUALIVE_mType, pAddress, pLength, (void*)0);
	txByte=sendMessage(message);

	freeMessage(message);
	
	message = newMessage(0, 0, 0, (void*)0);
	txByte=recvMessage(message);
	
	if ((txByte >= headerSIZE) && (txByte == (headerSIZE+message->pLength))){
		// Check if this message arrive from pAddress
		if (message->mAddress == pAddress) {
			if (message->mType == ACK_mType)
				retVal = sdepOK;

			if ((message->mType == ERROR_mType) && (message->pLength == 0)) 
				retVal = -sdepBADMESSAGE;
		}
	}

	freeMessage(message);

	return retVal;
}

/**
 * @brief	Send a BYEBYE message to the Slave
 * @param	pAddress - address of selected probe where to send and receive
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t sendDisconnect(mAddress_t pAddress){
	sdepMessage_t *message;
	uint16_t pLength;
	uint32_t txByte;
	
	int8_t retVal = -sdepERROR;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	pLength = 0;
		
	message = newMessage(BYEBYE_mType, pAddress, pLength, (void*)0);
	txByte=sendMessage(message);

	freeMessage(message);
	
	message = newMessage(0, 0, 0, (void*)0);
	txByte=recvMessage(message);
	
	if ((txByte >= headerSIZE) && (txByte == (headerSIZE+message->pLength))){
		// Check if this message arrive from pAddress
		if (message->mAddress == pAddress) {
			if (message->mType == ACK_mType)
				retVal = sdepOK;

			if ((message->mType == ERROR_mType) && (message->pLength == 0)) 
				retVal = -sdepBADMESSAGE;
		}
	}

	freeMessage(message);

	return retVal;
}


/**
 * @brief	Send the request of capability to a specified slave PROBE through the its address
 * @param	pAddress - address of selected probe where to send and receive
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t sendCapabilityRequest(mAddress_t pAddress){
	sdepMessage_t *message;
	uint16_t pLength;
	uint32_t txByte;
	
	int8_t retVal;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	pLength = 0;
		
	message = newMessage(WHAT_mType, pAddress, pLength, (void*)0);
	txByte=sendMessage(message);

	freeMessage(message);
	
	retVal = sdepCoreResponseDecoder(pAddress);
	
	switch(retVal) {
		case MYCAP_mType:
			return sdepOK;
			break;
		case -sdepNODEVADDR:
			return -sdepNODEVADDR;
			break;
		case -sdepBADMESSAGE:
			return -sdepBADMESSAGE;
			break;
		default:
			return -sdepBADRESPONSE;
			break;
	}
}

/**
 * @brief	Send the config list of a specified PROBE through the its address
 * @param	pAddress - address of selected probe where to send and receive
 * @param	sConfList[] - an array list of sConfig_t field
 * @param	scNum - number of sConfig_t field
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t sendConfigTo(mAddress_t pAddress, sConfig_t sConfList[], uint16_t scNum){
	sdepMessage_t *message;
	uint16_t pLength;
	uint32_t txByte;
	
	int8_t retVal;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	pLength = (scNum * sizeof(sConfig_t));
	if (pLength > pSIZE_MAX)
		return -sdepPAYTOOBIG;
			
	message = newMessage(CONF_mType, pAddress, pLength, (mPayload_t*)sConfList);
	txByte=sendMessage(message);

	freeMessage(message);
	
	retVal = sdepCoreResponseDecoder(pAddress);
	
	switch(retVal) {
		case ACK_mType:
		case ERROR_mType:
			return sdepOK;
			break;
		case -sdepNODEVADDR:
			return -sdepNODEVADDR;
			break;
		case -sdepBADMESSAGE:
			return -sdepBADMESSAGE;
			break;
		default:
			return -sdepBADRESPONSE;
			break;
	}
}

/**
 * @brief	Send the request of samples packet to a specified slave PROBE:SID through the its address
 * @param	pAddress - address of selected probe where to send and receive
 * @param	sensorId - sId_t field to identify a precise sensor on the probe
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t sendSamplesRequest(mAddress_t pAddress, sId_t sensorId){
	sdepMessage_t *message;
	sId_t recvSID;
	uint16_t pLength;
	uint32_t txByte;
	
	int8_t retVal;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	pLength = sizeof(sId_t);
		
	message = newMessage(DREQ_mType, pAddress, pLength, (mPayload_t*)&sensorId);
	txByte=sendMessage(message);

	freeMessage(message);
	
	retVal = sdepCoreResponseDecoder(pAddress);
	
	switch(retVal) {
		case DEXG_mType:
		case ERROR_mType:
			return sdepOK;
			break;
		case -sdepNODEVADDR:
			return -sdepNODEVADDR;
			break;
		case -sdepBADMESSAGE:
			return -sdepBADMESSAGE;
			break;
		default:
			return -sdepBADRESPONSE;
			break;
	}
}


/**
 * @brief	Send the config list of a specified PROBE through the its address
 * @param	pAddress - address of selected probe where to send and receive
 * @param	mPayload - custom payload to send
 * @param	pLength - size in byte of custom payload to send
 * @retval	sdepOK if success, else -sdepNODEVADDRESS if is a bad address, else -sdepBADMESSAGE, -sdepBADRESPONSE
 */
int8_t sendCustom(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength){
	sdepMessage_t *message;
	uint32_t txByte;
	
	int8_t retVal;
	
	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	if (pLength > pSIZE_MAX)
		return -sdepPAYTOOBIG;
			
	message = newMessage(CUSTOM_mType, pAddress, pLength, mPayload);
	txByte=sendMessage(message);

	freeMessage(message);
	
	retVal = sdepCoreResponseDecoder(pAddress);

	switch(retVal) {
		case CUSTOM_mType:
			return sdepOK;
			break;
		case -sdepNODEVADDR:
			return -sdepNODEVADDR;
			break;
		case -sdepBADMESSAGE:
			return retVal;
			break;
		default:
			return -sdepBADRESPONSE;
			break;
	}
}
