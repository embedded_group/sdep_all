/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_MESSAGE_H
#define SDEP_MESSAGE_H

#define	WHO_mType			0x01
#define	IAM_mType			0x02
#define	YOUARE_mType		0x03
#define	ACK_mType			0x04
#define	AREYOUALIVE_mType	0x05
#define	BYEBYE_mType		0x06
#define	WHAT_mType			0x07
#define	MYCAP_mType			0x08
#define	CONF_mType			0x09
#define	DREQ_mType			0x0A
#define	DEXG_mType			0x0B
#define	ERROR_mType			0x0C
#define	CUSTOM_mType		0x0F

#define mTypeSIZE			(sizeof(mType_t))
#define mTypeOFFSET			(mTypeSIZE)
#define mAddressSIZE		(sizeof(mAddress_t))
#define mAddressOFFSET		(mTypeSIZE+mAddressSIZE)
#define pLengthSIZE			(sizeof(pLength_t))
#define pLengthOFFSET		(mTypeSIZE+mAddressSIZE+pLengthSIZE)
#define headerSIZE			(mTypeSIZE+mAddressSIZE+pLengthSIZE)

// Sizing definitions
typedef uint8_t		mType_t;
typedef uint8_t		mAddress_t;
typedef uint16_t	pLength_t;
typedef uint8_t		mPayload_t;

typedef struct {
	mType_t		mType;
	mAddress_t	mAddress;
	pLength_t	pLength;
	mPayload_t	*mPayload;
} sdepMessage_t;

#define pSIZE_MAX	(1<<(8*sizeof(pLength_t)))

sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload);
void freeMessage(sdepMessage_t *sdepMessage);
uint32_t sendMessage(sdepMessage_t *sdepMessage);
uint32_t recvMessage(sdepMessage_t *sdepMessage);
#endif //SDEP_PROTOCOL_H
