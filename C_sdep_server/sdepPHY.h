/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_CORE_H
#define SDEP_CORE_H
#include "sdepMessage.h"
/* Define of the IOCTL message */
#define IOCTL_MAGIC      'U'
// Use this iotctl command for get a quantity of attached device
#define IOCTL_HOWMANYDEVICE  _IO(IOCTL_MAGIC, 0) // With no parameters
// Use this iotctl command for get a list of attached device
#define IOCTL_GETLIST  	     _IOW(IOCTL_MAGIC, 1, char*) // From kernel module to user process

typedef struct {
	uint8_t pAddress;
	uint32_t pSerial;
} sdepAddressList_f;

int8_t getAttachedCount(int filed, mAddress_t *howMany);
int8_t getAttachedList(int filed, sdepAddressList_f *sdepAddressList);
#endif //SDEP_CORE_H
