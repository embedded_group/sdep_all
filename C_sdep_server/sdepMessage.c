/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "sdepMessage.h"
#include "sdepCore.h"

extern sdepCore_t sdepCore;

#if useMALLOC == 0
// --------------------------------------------------------------------
// ---------------- Version without use of the MALLLOC ----------------
// --------------------------------------------------------------------

/**
 * @brief	Create and initialize a new sdepMessage
 * @param	mType - type of message (see the define)
 * @param	mAddress - address of the PROBE
 * @param	pLength - length of the payload
 * @param	mPayload - initialized payload if any or null reference
 * @retval	A reference to a well formatted sdepMessage
 */
sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload){
	static sdepMessage_t smessage;
	static mPayload_t smPayload[pSIZE_MAX];
		
	smessage.mType = mType;
	smessage.mAddress = mAddress;
	smessage.pLength = pLength;
	
	smessage.mPayload = smPayload;

	if ((smessage.pLength>0) && (mPayload!=(void*)0)) {
		memcpy(smessage.mPayload, mPayload, pLength);
	}

	return &smessage;
}

/**
 * @brief	In the non malloc mode, this function is empty but is define for symmetry
 * @param	sdepMessage - a previous allocated sdepMessage
 * @retval	None
 */
void freeMessage(sdepMessage_t *sdepMessage){
}

/**
 * @brief	Send a previous allocated well formatted sdepMessage trough the PHY
 * @param	sdepMessage - a well formatted sdepMessage
 * @retval	Number of sending byte
 */
uint32_t sendMessage(sdepMessage_t *sdepMessage){
	uint32_t sBytes;
	
	sBytes=sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, headerSIZE);
	if (sdepMessage->pLength>0) {
		sBytes+=sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage->mPayload, sdepMessage->pLength);
	}

	return sBytes;
}

/**
 * @brief	Receive a previous allocated well formatted sdepMessage trough the PHY
 * @param	sdepMessage - a well formatted sdepMessage
 * @retval	Number of received byte
 */
uint32_t recvMessage(sdepMessage_t *sdepMessage){
	uint32_t rBytes;

	rBytes=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, headerSIZE);
	if (sdepMessage->pLength>0) {
		rBytes+=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage->mPayload, sdepMessage->pLength);
	}
	
	return rBytes;
}

#else
// -----------------------------------------------------------------
// ---------------- Version with use of the MALLLOC ----------------
// -----------------------------------------------------------------

/**
 * @brief	Create and initialize a new sdepMessage
 * @param	mType - type of message (see the define)
 * @param	mAddress - address of the PROBE
 * @param	pLength - length of the payload
 * @param	mPayload - initialized payload if any or null reference
 * @retval	A reference to a well formatted sdepMessage
 */
sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload){
	sdepMessage_t *dmessage = (sdepMessage_t*)malloc(sizeof(sdepMessage_t));
	mPayload_t *dmPayload = (mPayload_t*)malloc(pLength);
	
	dmessage->mType = mType;
	dmessage->mAddress = mAddress;
	dmessage->pLength = pLength;
	
	dmessage->mPayload = dmPayload;

	if ((dmessage->pLength>0) && (mPayload!=(void*)0)) {
		memcpy(dmessage->mPayload, mPayload, pLength);
	}

	return dmessage;
}

/**
 * @brief	In the malloc mode, this function free the heap memory
 *          from an allocated sdepMessage
 * @param	sdepMessage - a previous allocated sdepMessage
 * @retval	None
 */
void freeMessage(sdepMessage_t *sdepMessage){
	if (sdepMessage != (void*)0) {
		if (sdepMessage->mPayload != (void*)0) {
			free(sdepMessage->mPayload);
		}
		free(sdepMessage);
	}
}

/**
 * @brief	Send a previous allocated well formatted sdepMessage trough the PHY
 * @param	sdepMessage - a well formatted sdepMessage
 * @retval	Number of sending byte
 */
uint32_t sendMessage(sdepMessage_t *sdepMessage){
	uint32_t sBytes;
	
	sBytes=sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage, headerSIZE);
	if (sdepMessage->pLength>0) {
		sBytes+=sdepCore.phyDriver.writeBytes((uint8_t*)sdepMessage->mPayload, sdepMessage->pLength);
	}

	return sBytes;
}

/**
 * @brief	Receive a previous allocated well formatted sdepMessage trough the PHY
 * @param	sdepMessage - a well formatted sdepMessage
 * @retval	Number of received byte
 */
uint32_t recvMessage(sdepMessage_t *sdepMessage){
	uint32_t rBytes;

	rBytes=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage, headerSIZE);
	if (sdepMessage->pLength>0) {
		if (sdepMessage->mPayload == (void*)0) sdepMessage->mPayload=(mPayload_t*)malloc(sdepMessage->pLength);
		rBytes+=sdepCore.phyDriver.readBytes((uint8_t*)sdepMessage->mPayload, sdepMessage->pLength);
	}
	
	return rBytes;
}

#endif //useMALLOC
