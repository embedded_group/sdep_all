/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#include <string.h>

#include "sdepCore.h"
#include "sdepConfig.h"
#include "sdepProtocol.h"
#include "sdepMessage.h"

// ----------------------------------------
// ----------- sdepCore Private -----------
// ----------------------------------------

sdepCore_t sdepCore;

// ----------- Support Function -----------
sId_t getSensorIdFrom(sdepMessage_t *message);

// ----------------------------------------
// ----------------------------------------
// ----------------------------------------

/**
 * @brief	Initialize the sdepCore machine and attach the callback function.
 * @param	phyDriver - a phyDriver initialized callbacks
 * @param	sdepCallbacks - a sdepCore initialized callbacks
 * @retval	sdepOK if all is correct else -sdepERROR
 */
int8_t sdepInitCore(phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks){
	// Check the passed parameters
	if ((!phyDriver.selectAddress) || (!phyDriver.writeBytes) || (!phyDriver.readBytes))
		return -sdepERROR;

	if ((!sdepCallbacks.ackResponse)        || (!sdepCallbacks.errorResponse)       ||
		(!sdepCallbacks.capabilityResponse) || (!sdepCallbacks.dataArriveResponse))
		return -sdepERROR;

	// Attach the callbacks function of the driver
	sdepCore.phyDriver.selectAddress = phyDriver.selectAddress;
	sdepCore.phyDriver.writeBytes = phyDriver.writeBytes;
	sdepCore.phyDriver.readBytes = phyDriver.readBytes;

	// Attach the callbacks function of state machine
	sdepCore.sdepCallbacks.ackResponse = sdepCallbacks.ackResponse;
	sdepCore.sdepCallbacks.errorResponse = sdepCallbacks.errorResponse;
	sdepCore.sdepCallbacks.capabilityResponse = sdepCallbacks.capabilityResponse;
	sdepCore.sdepCallbacks.dataArriveResponse = sdepCallbacks.dataArriveResponse;
	sdepCore.sdepCallbacks.customResponse = sdepCallbacks.customResponse;
	
	return sdepOK;
}

/**
 * @brief	Execute a single step of a sdepCore state machine
 * @param	None
 * @retval	sdepOK or if a bad message is intercept its return -sdepBADMESSAGE
 */
int8_t sdepCoreResponseDecoder(mAddress_t pAddress){
	int8_t retVal = sdepOK;
	sdepMessage_t *sdepMessage;
	int32_t recvBytes;

	if (sdepCore.phyDriver.selectAddress(pAddress) != sdepOK)
		return -sdepNODEVADDR;

	sdepMessage = newMessage(0, 0, 0, (void*)0);

	recvBytes = recvMessage(sdepMessage);
	// Check if this message is a possible sdepMessage
	if (recvBytes >= headerSIZE) {
		// Check if sdepMessage is formatted correctly
		if (recvBytes == (headerSIZE+sdepMessage->pLength)){
				// Enter in the sdepCore response decoder
				retVal = sdepMessage->mType;
				switch(sdepMessage->mType){
					// Verify ACK message call the ackResponse() callback if the MASTER as defined it
					case ACK_mType:
						while (sdepCore.sdepCallbacks.ackResponse(sdepMessage->mAddress) != sdepOK);
						break;
					// Verify ERROR message and call the errorResponse() callback if the MASTER as defined it
					case ERROR_mType:
						while (sdepCore.sdepCallbacks.errorResponse(sdepMessage->mAddress, (sError_t*)sdepMessage->mPayload, (sdepMessage->pLength/sizeof(sError_t))) != sdepOK);
						break;
					// Verify MYCAP message and call the capabilityResponse() callback if the MASTER as defined it
					case MYCAP_mType:
						while (sdepCore.sdepCallbacks.capabilityResponse(sdepMessage->mAddress, (sCapability_t*)sdepMessage->mPayload, (sdepMessage->pLength/sizeof(sCapability_t))) != sdepOK);
						break;
					// Verify DEXG message and call the dataArriveResponse() callback if the MASTER as defined it
					case DEXG_mType:
						while (sdepCore.sdepCallbacks.dataArriveResponse(sdepMessage->mAddress, getSensorIdFrom(sdepMessage), (uint8_t*)(sdepMessage->mPayload+sizeof(sId_t)), (sdepMessage->pLength-sizeof(sId_t))) != sdepOK);
						break;
					// Verify CUSTOM message and call the customRresponse() callback if the MASTER as defined it
					case CUSTOM_mType:
						if (sdepCore.sdepCallbacks.customResponse != (void*)0){
							while (sdepCore.sdepCallbacks.customResponse(sdepMessage->mAddress, sdepMessage->mPayload, sdepMessage->pLength) != sdepOK);
						}
						break;
					// Any other type of message or incorrect message are simply ignored.
					default:
						break;
				}

		} else {
			retVal = -sdepBADMESSAGE;
		}
	}

	freeMessage(sdepMessage);

	return retVal;
}

/**
 * @brief	Recover the sensorId from a payload of sdepMessage
 * @param	sdepMessage to analize
 * @retval	sensorId
 */
sId_t getSensorIdFrom(sdepMessage_t *message){
	return *((sId_t*)message->mPayload);
}
