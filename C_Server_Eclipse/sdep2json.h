/*
 * sdep2json.h
 *
 *  Created on: 03 lug 2016
 *      Author: rmaisto
 */

#include <stdint.h>
#include <json-c/json.h>
#include "sdepProtocol.h"
#include "sdepPHY.h"

#ifndef INC_SDEP2JSON_H_
#define INC_SDEP2JSON_H_

/**Conversion methods*/
json_object* sId_tJson(sId_t* sensorId);
json_object* sError_tJson(sError_t* error);
json_object* sCapability_tJson(sCapability_t* cap);
json_object* sExchange_tJson(sExchange_t* exchange, uint16_t arrSize);
json_object* sConfig_tJson(sConfig_t* config);
json_object* sAddress_fJson(sdepAddressList_f* address);
json_object* capList_Json(sCapability_t* capList, uint16_t arrSize);
json_object* errList_Json(sError_t* errList,uint16_t arrSize);
json_object* confList_Json(sConfig_t* confList,uint16_t arrSize);
json_object* addrList_Json(sdepAddressList_f* addrList, uint16_t arrSize);

#endif /* INC_SDEP2JSON_H_ */
