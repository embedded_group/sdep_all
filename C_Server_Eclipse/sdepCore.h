/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_CORE_H
#define SDEP_CORE_H

#include <stdint.h>
#include <stdlib.h>

#include "sdepConfig.h"
#include "sdepProtocol.h"
#include "sdepMessage.h"

// SDEP returned function value
#define sdepOK			0x00
#define sdepERROR		0x01
#define sdepBADMESSAGE	0x02
#define sdepPAYTOOBIG	0x03
#define sdepNODEVADDR	0x04
#define sdepBADRESPONSE 0x05

// SDEP internal define
#define sdepBADDRESS 	0xFF

typedef struct {
	int32_t (*selectAddress)(uint8_t pAddress);
	int32_t (*writeBytes)(const uint8_t *buffer, uint16_t length);
	int32_t (*readBytes)(uint8_t *buffer, uint16_t length);
} phyDriver_t;

typedef struct {
	int8_t (*ackResponse)(mAddress_t pAddress);
	int8_t (*errorResponse)(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum);
	int8_t (*capabilityResponse)(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
	int8_t (*dataArriveResponse)(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
	int8_t (*customResponse)(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength);
} sdepCallbacks_t;

typedef struct {
	sdepCallbacks_t sdepCallbacks;
	phyDriver_t phyDriver;
} sdepCore_t;

int8_t sdepInitCore(phyDriver_t phyDriver, sdepCallbacks_t sdepCallbacks);
int8_t sdepCoreResponseDecoder(mAddress_t pAddress);

#endif //SDEP_CORE_H
