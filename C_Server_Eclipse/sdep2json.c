/*
 * sdep2json.c
 *
 *  Created on: 03 lug 2016
 *      Author: rmaisto
 */

#include "sdep2json.h"
#include <json-c/json_object.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Converts a sId_t struct into a JSON object and returns the string of it
 * @param sensorId - pointer to the struct to convert
 * @retval pointer to JSON string
 */
json_object* sId_tJson(sId_t* sensorId)
{
	json_object *jobj = json_object_new_object();
	json_object *jsensType = json_object_new_int(sensorId->sensorType);
	json_object *jsensNum = json_object_new_int(sensorId->sensorNum);
	json_object_object_add(jobj,"sensorType",jsensType);
	json_object_object_add(jobj,"sensorNum",jsensNum);
	return jobj;
}

/**
 * @brief Converts a sError_t struct into a JSON object returns the string of it
 * @param error - pointer to the struct to convert
 * @retval pointer to JSON string
 */
json_object* sError_tJson(sError_t* error)
{
	json_object *jobj = json_object_new_object();
	json_object *jerrorCode = json_object_new_int(error->errorCode);
	json_object_object_add(jobj,"sensorId",sId_tJson(&(error->sensorId)));
	json_object_object_add(jobj,"errorCode",jerrorCode);
	return jobj;
}

/**
 * @brief Converts a sCapability_t struct into a JSON object returns the string of it
 * @param cap - pointer to the struct to convert
 * @retval pointer to JSON string
 */
json_object* sCapability_tJson(sCapability_t* cap)
{
	json_object* jobj = json_object_new_object();
	json_object* jsensType = json_object_new_int(cap->sensorType);
	json_object* jsensQuant = json_object_new_int(cap->sensorQuantity);
	json_object* jsamLen = json_object_new_int(cap->sampleLen);
	json_object_object_add(jobj,"sensorType",jsensType);
	json_object_object_add(jobj,"sensorQuantity",jsensQuant);
	json_object_object_add(jobj,"sampleLen",jsamLen);
	return jobj;
}

/**
 * @brief Converts a sExchange_t struct into a JSON object and returns the string of it
 * @param exchange - pointer to the struct to convert
 * @param arrSize - dimension of the sExchange.samplesPacket array field
 * @retval pointer to JSON string
 */
json_object* sExchange_tJson(sExchange_t* exchange, uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jsamArr = json_object_new_array();

	for(i = 0; i < arrSize; i++){
		json_object* jint = json_object_new_int(exchange->samplesPacket[i]);
		json_object_array_add(jsamArr,jint);
	}
	json_object_object_add(jobj,"sensorId",sId_tJson(&(exchange->sensorId)));
	json_object_object_add(jobj,"samplesPacket",jsamArr);
	return jobj;
}

/**
 * @brief Converts a sConfig_t struct into a JSON object and returns the string of it
 * @param config - pointer to the struct to convert
 * @retval pointer to JSON string
 */
json_object* sConfig_tJson(sConfig_t* config)
{
	json_object* jobj = json_object_new_object();
	json_object* jsensMod = json_object_new_int(config->sensorMode);
	json_object* jsensPer = json_object_new_int(config->sensorPeriod);

	json_object_object_add(jobj,"sensorId",sId_tJson(&(config->sensorId)));
	json_object_object_add(jobj,"sensorMode",jsensMod);
	json_object_object_add(jobj,"sensorPeriod",jsensPer);

	return jobj;
}

/**
 * @brief Converts a sdepAddressList_f struct into a JSON object and returns the string of it
 * @param address - pointer to the struct to convert
 * @retval pointer to JSON string
 */
json_object* sAddress_fJson(sdepAddressList_f* address)
{
	json_object* jobj = json_object_new_object();
	json_object* jAddr = json_object_new_int(address->pAddress);
	json_object* jSerial = json_object_new_int(address->pSerial);

	json_object_object_add(jobj,"pAddress",jAddr);
	json_object_object_add(jobj,"pSerial",jSerial);

	return jobj;
}

/**
 * @brief Converts an array of sCapability_t struct into a JSON object and returns the string of it
 * @param capList - pointer to the array struct to convert
 * @param arrSize - dimension of the array
 * @retval pointer to JSON string
 */
json_object* capList_Json(sCapability_t* capList, uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sCapability_tJson(&capList[i]));
	json_object_object_add(jobj,"CapabilityList",jArr);
	return jobj;
}

/**
 * @brief Converts a sError_t struct array into a JSON object and returns the string of it
 * @param errList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON string
 */
json_object* errList_Json(sError_t* errList,uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for( i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sError_tJson(&errList[i]));
	json_object_object_add(jobj,"ErrorList",jArr);
	return jobj;
}

/**
 * @brief Converts a sConfig_t struct array into a JSON object and returns the string of it
 * @param confList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON string
 */
json_object* confList_Json(sConfig_t* confList,uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sConfig_tJson(&confList[i]));
	json_object_object_add(jobj,"ConfigList",jArr);
	return jobj;
}

/**
 * @brief Converts a sdepAddressList_f struct array into a JSON object and returns the string of it
 * @param addrList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON string
 */
json_object* addrList_Json(sdepAddressList_f* addrList, uint16_t arrSize){
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sAddress_fJson(&addrList[i]));
	json_object_object_add(jobj,"AddressList",jArr);
	return jobj;
}
