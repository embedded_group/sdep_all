################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sdep2json.c \
../sdepCore.c \
../sdepMessage.c \
../sdepProtocol.c \
../server.c \
../server_f.c 

OBJS += \
./sdep2json.o \
./sdepCore.o \
./sdepMessage.o \
./sdepProtocol.o \
./server.o \
./server_f.o 

C_DEPS += \
./sdep2json.d \
./sdepCore.d \
./sdepMessage.d \
./sdepProtocol.d \
./server.d \
./server_f.d 


# Each subdirectory must supply rules for building sources it contributes
sdep2json.o: ../sdep2json.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I/usr/local/lib -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"sdep2json.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


