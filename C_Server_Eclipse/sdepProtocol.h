/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_PROTOCOL_H
#define SDEP_PROTOCOL_H

typedef struct {
	uint8_t sensorType;		//STYP
	uint8_t sensorNum;		//SNUM
} sId_t;

typedef struct {
	sId_t	sensorId;		//SID
	uint8_t	errorCode;		//ECOD
} sError_t;

typedef struct {
	uint8_t	sensorType;		//STYP
	uint8_t	sensorQuantity;	//SQTY
	uint8_t sampleLen;		//SLEN
} sCapability_t;

typedef struct {
	sId_t	sensorId;		//SID
	uint8_t	*samplesPacket;	//N0, N1, N2, ...
} sExchange_t;

typedef struct {
	sId_t	sensorId;		//SID
	uint8_t	sensorMode;		//MODE
	uint8_t sensorPeriod;	//PERIOD
} sConfig_t;

int8_t checkAlive(uint8_t pAddress);
int8_t sendDisconnect(uint8_t pAddress);
int8_t sendCapabilityRequest(uint8_t pAddress);
int8_t sendConfigTo(uint8_t pAddress, sConfig_t sConfList[], uint16_t scNum);
int8_t sendSamplesRequest(uint8_t pAddress, sId_t sensorId);
int8_t sendCustom(uint8_t pAddress, uint8_t *mPayload, uint16_t pLength);

#endif //SDEP_PROTOCOL_H

