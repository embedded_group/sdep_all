/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_CONFIG_H
#define SDEP_CONFIG_H

// If is set the malloc function is used for the sdepMessage
#define useMALLOC	1

#endif //SDEP_CONFIG_H
