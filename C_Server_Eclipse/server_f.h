#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <json/json.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "sdepProtocol.h"
#include "sdepMessage.h"
#include "sdep2json.h"

#define C_ALIVE 0
#define S_DISCONNECT 1
#define S_CAPABILITY 2
#define S_CONFIG 3
#define S_SAMPLE_REQ 4
#define CLOSE 5

json_object *jresp;


//Callbacks
void capabilityResponse (mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
