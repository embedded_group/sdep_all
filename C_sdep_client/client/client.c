#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <json/json.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>


int main()
{
	char* str;
	int fd = 0;
	struct sockaddr_in demoserverAddr;

	fd = socket(AF_INET, SOCK_STREAM, 0);

	if (fd < 0)
	{
	    printf("Error : Could not create socket\n");
	    return 1;
	}
	else
	{
	    demoserverAddr.sin_family = AF_INET;
	    demoserverAddr.sin_port = htons(7098);
	    demoserverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	    memset(demoserverAddr.sin_zero, '\0', sizeof(demoserverAddr.sin_zero));
	}

	if (connect(fd, (const struct sockaddr *)&demoserverAddr, sizeof(demoserverAddr)) < 0)
	{
	     printf("ERROR connecting to server\n");
	     return 1;
	}

	/*Creating a json object*/
	json_object *jobj = json_object_new_object();


	/*Creating a json integer*/
	json_object *jint = json_object_new_int(2);

	/*Creating a json integer*/
	json_object *jint1 = json_object_new_int(3);

	/*Creating a json integer*/
	json_object *jint2 = json_object_new_int(10);

	/*Creating a json integer*/
	json_object *jint3 = json_object_new_int(10);

	/*Form the json object*/
	/*Each of these is like a key value pair*/

	json_object_object_add(jobj,"Method", jint);
	json_object_object_add(jobj,"pAddress", jint1);
	json_object_object_add(jobj,"sensorType", jint2);
	json_object_object_add(jobj,"sensorNum", jint3);

	printf("Size of JSON object- %lu\n", sizeof(jobj));
	printf("Size of JSON_TO_STRING- %lu,\n %s\n", sizeof(json_object_to_json_string(jobj)), json_object_to_json_string(jobj));


    char temp_buff[3000];

    if (strcpy(temp_buff, json_object_to_json_string(jobj)) == NULL)
    {
        perror("strcpy");
        return EXIT_FAILURE;
    }

    if (send(fd, temp_buff, strlen(temp_buff), 0) == -1)
    {
        perror("write");
        return EXIT_FAILURE;
    }


    printf("Written data\n");

    /********************************************************/
    char buff[1000];

    	int n ;

    n = recv(fd, buff, 1024, 0) ;
    if(n < 0)
        {
            perror("write");
            return EXIT_FAILURE;
        }




    printf("tu madre: %s\n",buff);
    json_object * jobj1 =json_tokener_parse(buff);

    printf("Size of JSON object- %lu\n", sizeof(jobj1));
    printf("Size of JSON_TO_STRING- %lu,\n %s\n", sizeof(json_object_to_json_string(jobj1)), json_object_to_json_string(jobj1));

    return EXIT_SUCCESS;
}
